# Synth

Synth related scripts can be found by the `omni_synth` prefix of various scripts
located on your `PATH` after a successful `omni` install.

This page contains help text for all Synth related processing scripts. You can also see a copy of these texts by invoking the `-h` flag on the relevant script call.

## Pipelines
```{toctree}
---
maxdepth: 2
---
pipelines/index
```

## Utilities
```{toctree}
---
maxdepth: 2
---
utilities/index
```
