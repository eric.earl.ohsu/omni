**omni_synthregressionmask**
============================

Overview
--------
``omni_synthregressionmask`` is a utility designed to assist with generating the weight images used by ``omni_synthtarget`` when estimating synthetic EPI images. ``omni_synthregressionmask`` is used in the **omni** preprocessing scripts, ``omni_synthpipeline`` and ``omni_synthpreproc``. The goal of a good weight image is to reduce the contribution of very noisy or distorted areas when estimating a synthetic image. Properly created, a good weight mask can significantly improve the accuracy of the contrast properties of a synthetic image. Good synthetic EPI contrast accuracy is key to successfully performing field map-less distortion correction. Understanding how ``omni_synthregressionmask`` works may be helpful for users who are incorporating field map-less distortion correction into a pre-exisiting MRI preprocessing pipeline or dealing with tricky registration cases where default parameters are not performing optimally.

.. note::

	Correctly estimating the voxel intensity mapping needed to create a synthetic EPI image is very difficult when source and and target images are distorted differently. This is because in highly distorted areas corresponding voxels in source and target images may no longer represent the same portion of the imaged object. When this occurs, voxels in the mismatched portions of the target and source images can no longer be accurately modelled by the true source(s)-to-target intensity mapping function. Such areas are effectively *noise* and tend to reduce the contrast of the resulting synthetic image. 

	Also, it is often desirable to bias the estimate of the synthetic EPI image so that it favors the accurate representation of some particular region of the target image. For instance, when creating a synthetic EPI image intended for field map-less distortion correcton, you may want to ensure that the contrast properies of the part of the synthetic image containing the brain is the most accurate, and permit relatively lower contrast accuracy in the region on the synthetic image containing the neck and eyeballs. This can be accomplished by providing ``omni_synthtarget`` with an appropriate weight image, such as the one produced by ``omni_synthregressionmask``.

 
Example commands
----------------
.. code-block:: shell
	
	# Example 1: 
	omni_synthregressionmask -e ~/data/EPIData.nii.gz -m ~/data/AnatomicalBETMask.nii.gz -a Anat2EPI.1D -o ~/data/RegressionMask.nii.gz

	# Example 2: 
	omni_synthregressionmask -e ~/data/EPIData.nii.gz -m ~/data/AnatomicalBETMask.nii.gz -a Anat2EPI.1D -o ~/data/RegressionMask.nii.gz -f Anat2EPINonLinearWarp.nii.gz -s 6


Illustration
------------
.. figure:: ../../_static/figures/omni_synthregressionmask_figure_01.png

	**Example weight mask produced by omni_synthregressionmask.** A weight mask emphasizing the contribution of brain voxels. Reduced weighting is depicted in red, increased weighting is depicted in yellow. ``omni_synthregressionmask`` produces weight masks that reduce the contribution of noise regions (internally detected with ``omni_synthnoisemask``) and increase the contribution of brain voxels relative to immediately the surrounding skull. 

Arguments
---------

Required arguments
++++++++++++++++++

.. add_argument:: omni.scripts.synthregressionmask dil_anat_bet_mask

.. add_argument:: omni.scripts.synthregressionmask anat_bet_mask

.. add_argument:: omni.scripts.synthregressionmask epi

.. add_argument:: omni.scripts.synthregressionmask anat_to_epi_affine

.. add_argument:: omni.scripts.synthregressionmask output_path

Optional arguments
++++++++++++++++++

.. add_argument:: omni.scripts.synthregressionmask  warp

.. add_argument:: omni.scripts.synthregressionmask  noise_mask_dilation_size

.. add_argument:: omni.scripts.synthregressionmask  noise_mask_iterations

.. add_argument:: omni.scripts.synthregressionmask  noise_mask_sigma

.. add_argument:: omni.scripts.synthregressionmask  version

.. add_argument:: omni.scripts.synthregressionmask  help

