**omni_synthtarget**
====================

Overview
--------

The SynthTarget algorithm. Solves the joint synthetic transformation/affine registration problem. See algorithm 1 of
the Synth paper.

Example commands
----------------

.. code-block:: shell

    # Example 1
    omni_synthtarget EPI.nii.gz 'rbf(0;4)+rbf(1;10)' Tw1.nii.gz T2w.nii.gz

    # Example 2
    Somni_synthtarget EPI.nii.gz 'rbf(0;5)+rbf(1;5)' Tw1.nii.gz T2w.nii.gz \
    -p 16 -c -u ToneCurveMask.nii.gz

Argument types
----------------

In order to run, ``omni_synthtarget`` requires a minimum of three positional input arguments.

Required arguments
------------------

.. add_argument:: omni.scripts.synthtarget target_image

.. note::
    The presence of extreme intensity outliers in either the target image or the set of input images can reduce the
    performance of quality of synthetic images produced by SynthTarget. We recommend that input images be "despiked"
    prior to use. One approach to this is to measure the distribution of voxel intensities and "clip" extreme values
    that exceed some statistical threshold, say 3 standard deviations. Alternatively images can be run through an
    appropriate sigmoidal softmax function to compress extreme values. Other spatial denoising approaches may prove
    superior to either of these methods.

.. note::
    It is highly recommended that the target image is rescaled so that the minimum value at any voxel is 0 and the
    highest value is 1. While synth_target will happily run without that rescaling, the default values for error
    tolerances and gradient step size are unlikely to be optimal and model convergence will be something of a crapshoot.

.. note::
    We recommend that all images used for the target_image or as an input_image be properly debiased. The presence
    of bias fields can have a significant and deleterious effect on the accuracy of synthetic image estimation. If the
    synthetic image output looks wacky, closely examining the images for residual low spatial frequency intensity
    modulation should be one of the first troubleshooting steps taken. In the current pipeline, synthpreproc, we use
    N4BiasFieldCorrection to accomplish this. Often the knot spacing on the default spline model must be adjusted to be
    appropriate for the input data set.

.. note::
    Image contrast for T1w, T2w and EPI images can vary greatly depending on acquisition parameters. For situations
    in which image contrast is low --that is when the differences between the average intensities of voxels representing
    different tissues and fluids are minimal-- one can run into a problem with the radial basis function decomposition
    in which the majority of the image is represented by a single radial basis function component. In this situation the
    resulting model is not flexible enough to correctly model the intensity mapping relationship between source and
    target images. A solution could be to increase the order of the radial basis function decomposition, effectively
    increasing the number of intensity bins to divide that divide up the source images. However, going this route so can
    also significantly increase memory usage and slow performance. Often, a better solution is to perform a histogram
    normalization step on the source and target images. This procedure essentially "stretches" the distribution of voxel
    intensities in the images so that they are more evenly spaced along the interval of maximum and minimum values.
    Histogram normalized images can be more effectively decomposed by a given number of radial basis function
    components. We recommend that this procedure, or something similar, be applied to all images used as input to
    SynthTarget.

.. add_argument:: omni.scripts.synthtarget interaction_model

.. add_argument:: omni.scripts.synthtarget input_images

Optional arguments
------------------

.. add_argument:: omni.scripts.synthtarget regression_weight_mask

.. add_argument:: omni.scripts.synthtarget affine

.. add_argument:: omni.scripts.synthtarget output_prefix

.. note::
    If your intent is to iteratively re-estimate a synthetic image after unwarping a distorted target (as in
    SynthUnwarp), it is essential that you employ the -a and --no_register flag after the initial registration. Failing
    to do so can result in a situation where small errors in the affine alignement of the synthetic image are
    "corrected" by the subsequent non-linear registration step. The partially corrected target image will then have this
    affine error baked into it. If SynthTarget is run again using the new target image, and is allowed to reestimate the
    affine parameters, it will do so while adding some new affine error. This new error combines with the previous
    affine error baked in by the non-linear warping step. A chase then ensues in which affine errors from SynthTarget
    keep growing after each iteration only to be corrected by the non-linear warping step. Sufficiently iterated, large
    affine registration errors can develop. So make sure you use -a and --no_register if you are going to use
    SynthTarget in this way.

.. add_argument:: omni.scripts.synthtarget regression_output

.. add_argument:: omni.scripts.synthtarget synth_image

.. add_argument:: omni.scripts.synthtarget synth_image_target_space

.. add_argument:: omni.scripts.synthtarget err_tol

.. add_argument:: omni.scripts.synthtarget step_size

.. add_argument:: omni.scripts.synthtarget max_iterations

.. add_argument:: omni.scripts.synthtarget tone_curve

.. add_argument:: omni.scripts.synthtarget tone_curve_mask

.. note::
    We currently recommend constructing the TONE_CURVE_MASK by taking a binary brain-only mask image that is aligned to
    the input images and then dilating it out by some amount so that in encompasses the brain, surrounding CSF and an
    portions of the skull.

.. add_argument:: omni.scripts.synthtarget bandwidth

.. add_argument:: omni.scripts.synthtarget fixed_regress

.. add_argument:: omni.scripts.synthtarget no_register

.. add_argument:: omni.scripts.synthtarget output_intmat

.. add_argument:: omni.scripts.synthtarget number_of_threads

.. add_argument:: omni.scripts.synthtarget version

.. add_argument:: omni.scripts.synthtarget help
