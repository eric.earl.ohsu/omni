**omni_warp**
=============

Overview
--------
Script for converting/inverting nonlinear transforms.

Example commands
----------------
.. code-block:: shell
	
    omni_warp

Arguments
---------

Required arguments
++++++++++++++++++

.. add_argument:: omni.scripts.warp input

.. add_argument:: omni.scripts.warp output

Optional arguments
++++++++++++++++++

.. add_argument:: omni.scripts.warp in_warp_type

.. add_argument:: omni.scripts.warp out_warp_type

.. add_argument:: omni.scripts.warp target

.. add_argument:: omni.scripts.warp invert

.. add_argument:: omni.scripts.warp version

.. add_argument:: omni.scripts.warp help
