**omni_realign4d**
==================

Overview
--------

Example commands
----------------
.. code-block:: shell
	
    omni_realign4d

Arguments
---------

Required arguments
++++++++++++++++++

.. add_argument:: omni.scripts.realign4d fourd_img

.. add_argument:: omni.scripts.realign4d bids_sidecar

.. add_argument:: omni.scripts.realign4d output_prefix

Optional arguments
++++++++++++++++++

.. add_argument:: omni.scripts.realign4d ref_idx

.. add_argument:: omni.scripts.realign4d loops

.. add_argument:: omni.scripts.realign4d subsample

.. add_argument:: omni.scripts.realign4d config_file

.. add_argument:: omni.scripts.realign4d generate_config

.. add_argument:: omni.scripts.realign4d number_of_threads

.. add_argument:: omni.scripts.realign4d version

.. add_argument:: omni.scripts.realign4d help
