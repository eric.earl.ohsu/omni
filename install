#!/usr/bin/env python3
"""
    This script will install omni

    It exists due to inconsistencies in the pip installation
    process for local packages. It may be removed in the future
    once this process is more stable.
"""

import os
import sys
import argparse
import shutil
import subprocess

PROJECTDIR = os.path.dirname(os.path.abspath(__file__))

if __name__ == "__main__":
    print("This script is deprecated. To install omni simply call:\n" "`pip install /path/to/this/directory`")
    sys.exit(0)

    # create command line parser
    parser = argparse.ArgumentParser(
        description="Install Script for omni", epilog="Author: Andrew Van, vanandrew@wustl.edu, 04/22/2021"
    )
    parser.add_argument("-e", "--editable", dest="develop", action="store_true", help="same as -d/--develop")
    parser.add_argument("--develop", action="store_true", help="installs omni in editable mode (no PEP517)")
    parser.add_argument("-l", "--local", action="store_true", help="installs omni in .local")
    parser.add_argument("--user", dest="local", action="store_true", help="same as -l/--local")
    parser.add_argument("-t", "--test", action="store_true", help="installs test dependencies")
    parser.add_argument("-d", "--doc", action="store_true", help="installs doc dependecies")
    parser.add_argument("--skip_nipy", action="store_true", help="skips nipy install")
    parser.add_argument("--x86_64", action="store_true", help="uses x86-64 for march, instead of native")
    parser.add_argument("-v", "--verbose", action="store_true", help="verbose output")

    # call parser
    args = parser.parse_args()

    # check if git-lfs is installed
    try:
        # Make sure it is initialized
        subprocess.run(["git-lfs", "install"], check=True)
        # Make sure test data is pulled
        subprocess.run(["git-lfs", "pull"], check=True)
    except FileNotFoundError as e:
        raise IOError("git-lfs is required for binary data. You must install git-lfs.") from e

    # check python version
    python_version = sys.version_info

    # check if python at least 3.6
    if not python_version.major >= 3 and not python_version.minor >= 6:
        print("Python 3.6 or greater is required.")
        sys.exit(1)

    # check pip version
    try:
        pip_run = subprocess.run(
            ["python3", "-m", "pip", "--version"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True
        )
    except subprocess.CalledProcessError as subprocess_error:
        raise ValueError("pip is not installed. pip 19.1 or greater is required.") from subprocess_error
    pip_version = pip_run.stdout.decode("utf8").split("pip ")[1].split(" from")[0]
    pip_major = int(pip_version.split(".")[0])
    pip_minor = int(pip_version.split(".")[1])

    # check if at least 19.1
    if pip_major < 19 and pip_minor < 1:
        print("pip 19.1 or greater is required.")
        sys.exit(1)

    # get the current linux distro
    try:
        distro = subprocess.run(["cat", "/etc/lsb-release"], stdout=subprocess.PIPE, check=True)
        distrib_id = [line for line in distro.stdout.decode("utf-8").split("\n") if "DISTRIB_ID" in line][0].split("=")[
            1
        ]
        if distrib_id == "Ubuntu":  # if ubuntu set the hdf5 dir
            print("Distribution is Ubuntu, checking CPLUS_INCLUDE_PATH...")
            if "/usr/include/hdf5/serial" not in os.environ.get("CPLUS_INCLUDE_PATH", ""):
                print("Adding /usr/include/hdf5/serial")
                os.environ["CPLUS_INCLUDE_PATH"] = (
                    "/usr/include/hdf5/serial"
                    if os.environ.get("CPLUS_INCLUDE_PATH", "") == ""
                    else os.environ.get("CPLUS_INCLUDE_PATH", "") + ":/usr/include/hdf5/serial"
                )
        elif distrib_id == "Arch":
            print("Distro detected is Arch. Good Choice!")
    except subprocess.CalledProcessError:
        print("Could not parse linux distro!")

    # check cmake version
    try:
        cmake_run = subprocess.run(["cmake", "--version"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
    except subprocess.CalledProcessError as subprocess_error:
        raise ValueError("cmake is not installed. cmake 3.13 or greater is required.") from subprocess_error
    cmake_version = cmake_run.stdout.decode("utf8").split("version ")[1].split("\n")[0]
    cmake_major = int(cmake_version.split(".")[0])
    cmake_minor = int(cmake_version.split(".")[1])

    # check if cmake at least 3.13
    if cmake_major < 3 and cmake_minor < 13:
        print("cmake 3.13 or greater is required.")
        sys.exit(1)

    # check gcc version
    try:
        gcc_run = subprocess.run(["gcc", "--version"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True)
    except subprocess.CalledProcessError as subprocess_error:
        raise ValueError("gcc is not installed. gcc 7 or greater is required.") from subprocess_error
    gcc_version = gcc_run.stdout.decode("utf-8").split(") ")[1].split("\n")[0]
    gcc_major = int(gcc_version.split(".")[0])

    # check if gcc at least 7
    if gcc_major < 7:
        print("gcc 7 or greater is required.")
        sys.exit(1)

    # do a git submodule update --init (just in-case the user forgot)
    subprocess.run(["git", "submodule", "update", "--init"], stdout=True, check=True)

    # set environment variable for native
    if not args.x86_64:
        os.environ["COMPILE_ARCH"] = "native"

    # create pip call
    PIP_INSTALL = "python3 -m pip install"
    NIPY_INSTALL = PIP_INSTALL

    # install test extras if enabled
    INSTALL_DIR = "./"
    USE_EXTRA = False
    EXTRAS = list()
    if args.test:
        EXTRAS.append("test")
        USE_EXTRA = True
    if args.doc:
        EXTRAS.append("doc")
        USE_EXTRA = True
    if USE_EXTRA:
        extra_flags = ",".join(EXTRAS)
        INSTALL_DIR += "[%s]" % extra_flags

    # develop mode
    if args.develop:  # non-pep517 install
        PIP_INSTALL += " --editable %s --no-use-pep517" % INSTALL_DIR
    else:  # pep 517 install
        # remove folders from develop mode if they exist
        shutil.rmtree(os.path.join(PROJECTDIR, "build"), ignore_errors=True)
        shutil.rmtree(os.path.join(PROJECTDIR, "omni.egg-info"), ignore_errors=True)
        PIP_INSTALL += " %s" % INSTALL_DIR

    # local mode
    if args.local:
        PIP_INSTALL += " --user"
        NIPY_INSTALL += " --user"

    # verbose output
    if args.verbose:
        PIP_INSTALL += " --verbose"
        NIPY_INSTALL += " --verbose"

        # check if pyproject is in setup_ext (this can happen if a previous
        # install run was interrupted)
    if os.path.exists(os.path.join(PROJECTDIR, "setup_ext", "pyproject.toml")):
        # move it back to the PROJECTDIR
        shutil.move(os.path.join(PROJECTDIR, "setup_ext", "pyproject.toml"), os.path.join(PROJECTDIR))

    # run pip install
    if args.develop:  # move pyproject toml since incompatible with PEP517
        shutil.move(os.path.join(PROJECTDIR, "pyproject.toml"), os.path.join(PROJECTDIR, "setup_ext"))
    else:  # remove existing local build directory if it exists
        build_dir = os.path.join(PROJECTDIR, "build")
        if os.path.exists(build_dir):
            shutil.rmtree(build_dir)
    print("Running command: %s" % PIP_INSTALL)
    pip_finished = subprocess.run(PIP_INSTALL, shell=True, check=True)
    if args.develop:  # move back pyproject toml
        shutil.move(os.path.join(PROJECTDIR, "setup_ext", "pyproject.toml"), os.path.join(PROJECTDIR))
    if pip_finished.returncode != 0:
        print("Install Failed. Check output for details.")
        sys.exit(1)

    # install nipy
    if not args.skip_nipy:
        NIPY_INSTALL += " git+https://github.com/vanandrew/nipy.git@0.4.2_dep_fix"
        # nipy_finished = subprocess.run(NIPY_INSTALL, shell=True, check=True)

        # check if nipy install successful
        # if nipy_finished.returncode != 0:
        # print("nipy install failed. Check output for details.")
        # sys.exit(1)
    else:
        print("Skipping nipy install...")
